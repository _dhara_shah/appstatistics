# About Application #
AppStatistics is a demo application that gets a list of installed applications with the most used app at the top.
You get to know the most used app based on the launch count.

# How do I setup #
In order to run or play around with the source code of this application:
1. Install Android studio, any version will do
2. Install the Android SDK and JDK
3. Clone this repository and run this application like every other android application

# Testing #
The app has been tested on Android Lollipop and Marshmallow. The apk can be found within the app folder, and also under the build folder

# Known issues #
- At the moment the launch count is obtained using Reflection in Java,
since the field is hidden and might be removed later on as it is not that helpful.
- Pagination has not been implemented in this application

# Support #
- This application is compatible with Lollipop and Marshmallows
- For all smartphones excluding tablets
- Can be used in both landscape and portrait modes

# Dependencies #
There are no extra module dependencies in the project
The libraries used are:

1. Recycler view
2. Card View
3. Support library

# Contact #
In case of any queries you can get back to *sdhara2@hotmail.com*