package com.dhara.babajobs.appstats;

import android.app.Application;
import android.content.Context;

/**
 * Created by USER on 13-04-2016.
 */
public class MyAppStats extends Application {
    private static Context mContext;
    private static MyAppStats mApp;
    @Override
    public void onCreate() {
        super.onCreate();
        mContext=this;
        mApp=this;
    }

    public static MyAppStats getAppContext(){
        if(mApp == null) {
            mApp = (MyAppStats)mContext;
        }
        return mApp;
    }
}
