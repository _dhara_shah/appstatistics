package com.dhara.babajobs.appstats.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by USER on 13-04-2016.
 */
public class BaseActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
