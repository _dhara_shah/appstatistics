package com.dhara.babajobs.appstats.activities;

import android.app.usage.UsageStats;
import android.content.Intent;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.dhara.babajobs.appstats.MyAppStats;
import com.dhara.babajobs.appstats.R;
import com.dhara.babajobs.appstats.adapters.AppStatsCursorAdapter;
import com.dhara.babajobs.appstats.constants.Global;
import com.dhara.babajobs.appstats.customdialogs.CustomDialog;
import com.dhara.babajobs.appstats.db.DBHelper;
import com.dhara.babajobs.appstats.db.UsageStatsContentProvider;
import com.dhara.babajobs.appstats.db.UsageStatsContract;
import com.dhara.babajobs.appstats.utils.UsageStatsManager;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor>, CustomDialog.DismissListener {
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private AppStatsCursorAdapter mAppCursorAdapter;
    private List<UsageStats> mAppList;
    private ProgressBar mProgressBar;
    private CustomDialog.DismissListener mDismissListener;

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {

                case Global.HIDE_PROGRESS_BAR:
                    mProgressBar.setVisibility(View.GONE);
                    break;

                case Global.LIST_OBTAINED:
                    mProgressBar.setVisibility(View.GONE);
                    if(!mAppList.isEmpty()) {
                        getAppList();
                    }else {
                        CustomDialog.showCustomDialog(MainActivity.this,
                                mDismissListener);
                    }
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar();
        initViews();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case Global.REQUEST_PACKAGE_USAGE_STATS:
                getAppList();
                break;
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        String[] projection = { UsageStatsContract.UsageStatsEntry._ID,
                UsageStatsContract.UsageStatsEntry.COLUMN_NAME_APPLICATION_NAME,
                UsageStatsContract.UsageStatsEntry.COLUMN_NAME_PACKAGE_NAME,
                UsageStatsContract.UsageStatsEntry.COLUMN_NAME_LAST_TIME_USED,
                UsageStatsContract.UsageStatsEntry.COLUMN_NAME_LAUNCH_COUNT};

        CursorLoader cursorLoader = new CursorLoader(MainActivity.this,
                UsageStatsContentProvider.CONTENT_URI, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        /**
         * In order to perform pagination here, we can store an instance of this cursor
         * to a local variable use merge cursors and then swap the cursor with the merged cursor
         * For this example, we are moving ahead with no pagination
         */
        mAppCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAppCursorAdapter.swapCursor(null);
    }

    @Override
    public void onDismissed() {
        startActivityForResult(
                new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS),
                Global.REQUEST_PACKAGE_USAGE_STATS);
    }

    private void initToolbar(){
        /**
         * Initialize the toolbar and set the toolbar as the actionbar
         */
        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    private void initViews() {
        mAppList = new ArrayList<>();

        mDismissListener = this;

        /**
         * Initialize the views
         */
        mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
        mRecyclerView = (RecyclerView)findViewById(R.id.recylerView);

        mProgressBar.setVisibility(View.GONE);

        /**
         * Sets the recycler view and the adapter
         */
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(MyAppStats.getAppContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        /**
         * All data has been inserted into the database
         */
        getSupportLoaderManager().initLoader(Global.APP_STAT_LIST_LOADER, null, this);

        /**
         * Set the adapter to the recycler view
         */
        mAppCursorAdapter = new AppStatsCursorAdapter();
        mRecyclerView.setAdapter(mAppCursorAdapter);

        /**
         * Get the application list with the most used at the top
         */
        getUsageApplicationList();
    }

    private void getUsageApplicationList(){
        /**
         * Check if permissions have been granted, if so, get the list
         * else display the settings screen to grant the permission
         */
        new Thread(new Runnable() {
            @Override
            public void run() {
                mAppList = UsageStatsManager.getInstance().getListToCheckPermissions();
                handler.sendEmptyMessage(Global.LIST_OBTAINED);
            }
        }).start();
    }

    private void getAppList(){
        mProgressBar.setVisibility(View.VISIBLE);
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<UsageStats> usageStatsList = UsageStatsManager.getInstance().getUsageStatList();
                handler.sendEmptyMessage(Global.HIDE_PROGRESS_BAR);
                DBHelper.insertUsageStats(usageStatsList);
            }
        }).start();
    }
}
