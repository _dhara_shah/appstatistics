package com.dhara.babajobs.appstats.adapters;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dhara.babajobs.appstats.R;
import com.dhara.babajobs.appstats.adapters.viewholders.AppStatsViewHolder;

/**
 * Created by USER on 15-04-2016.
 */
public class AppStatsCursorAdapter extends RecyclerViewCursorAdapter<AppStatsViewHolder> {
    @Override
    public void onBindViewHolder(AppStatsViewHolder holder, Cursor cursor) {
        holder.bindData(cursor);
    }

    @Override
    public AppStatsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.individual_row,
                viewGroup, false);
        return new AppStatsViewHolder(view);
    }
}
