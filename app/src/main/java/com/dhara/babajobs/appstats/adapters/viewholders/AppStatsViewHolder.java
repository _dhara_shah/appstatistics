package com.dhara.babajobs.appstats.adapters.viewholders;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.dhara.babajobs.appstats.R;
import com.dhara.babajobs.appstats.constants.Global;
import com.dhara.babajobs.appstats.customviews.CustomTextView;
import com.dhara.babajobs.appstats.db.UsageStatsContract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by USER on 15-04-2016.
 */
public class AppStatsViewHolder extends RecyclerView.ViewHolder {
    public CustomTextView mTxtPackageName;
    public CustomTextView mTxtApplicationName;
    public CustomTextView mTxtLastTimeUsed;
    public CustomTextView mTxtLaunchCount;
    private SimpleDateFormat mSimpleDateFormat;

    public AppStatsViewHolder(View itemView) {
        super(itemView);
        mSimpleDateFormat = new SimpleDateFormat(Global.DATE_FORMAT);
        mTxtLastTimeUsed = (CustomTextView)itemView.findViewById(R.id.txtLastTimeUsed);
        mTxtPackageName = (CustomTextView)itemView.findViewById(R.id.txtPackageName);
        mTxtApplicationName = (CustomTextView)itemView.findViewById(R.id.txtApplicationName);
        mTxtLaunchCount = (CustomTextView)itemView.findViewById(R.id.txtLaunchCount);
    }

    public void bindData(Cursor cursor) {
        String dateTime = cursor.getString(cursor
                .getColumnIndex(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_LAST_TIME_USED));
        String packageName = cursor.getString(cursor
                .getColumnIndex(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_PACKAGE_NAME));
        String appName = cursor.getString(cursor
                .getColumnIndex(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_APPLICATION_NAME));
        int launchCount = cursor.getInt(cursor
                .getColumnIndex(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_LAUNCH_COUNT));

        mTxtPackageName.setText(packageName);
        mTxtApplicationName.setText(appName);
        mTxtLaunchCount.setText((launchCount == -1) ? "Not known" : launchCount+" times ");

        /**
         * Convert time to readable form
         */
        Date date = new Date(Long.parseLong(dateTime));
        mTxtLastTimeUsed.setText(mSimpleDateFormat.format(date));
    }
}
