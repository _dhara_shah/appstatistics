package com.dhara.babajobs.appstats.constants;

/**
 * Created by USER on 15-04-2016.
 */
public class Global {
    /**
     * Stores all the constants that may be needed in the application
     */
    public static final String DATE_FORMAT = "dd MMM yyyy hh:mm:ss a";

    public static final int REQUEST_PACKAGE_USAGE_STATS = 101;
    public static final int APP_STAT_LIST_LOADER = 102;
    public static final int LIST_OBTAINED=103;
    public static final int HIDE_PROGRESS_BAR=104;
}
