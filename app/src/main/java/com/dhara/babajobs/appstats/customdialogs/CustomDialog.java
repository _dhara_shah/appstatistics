package com.dhara.babajobs.appstats.customdialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.dhara.babajobs.appstats.MyAppStats;
import com.dhara.babajobs.appstats.R;

public class CustomDialog {
    /**
     * Listens to the dialog being dismissed when an "okay" is tapped on
     */
	public interface DismissListener {
		void onDismissed();
	}

    /**
     * Displays a dialog to the user
     * @param context
     * @param dismissListener
     * @return
     */
	public static AlertDialog showCustomDialog(Context context,
			final DismissListener dismissListener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context)
				.setTitle(null)
				.setMessage(MyAppStats.getAppContext().getString(R.string.you_need_permissions))
				.setPositiveButton(MyAppStats.getAppContext().getString(R.string.ok),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();

								/**
								 * Send a callback since the user accepts it
								 */
								if(dismissListener != null) {
									dismissListener.onDismissed();
								}
							}
						})
				.setNegativeButton(MyAppStats.getAppContext().getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});

		AlertDialog dialog = builder.create();
		dialog.show();
		return dialog;
	}
}
