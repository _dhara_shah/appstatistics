package com.dhara.babajobs.appstats.db;

import android.app.usage.UsageStats;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import com.dhara.babajobs.appstats.MyAppStats;
import com.dhara.babajobs.appstats.utils.UsageStatsManager;

import java.util.List;

/**
 * Created by USER on 14-04-2016.
 * Helper class that actually creates and manages
 * the provider's underlying data repository.
 */
public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "usagestats.sqlite";

    public static final String CREATE_USAGE_STAT_TABLE =
            " CREATE TABLE " + UsageStatsContract.UsageStatsEntry.TABLE_NAME +
            " (" + UsageStatsContract.UsageStatsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            UsageStatsContract.UsageStatsEntry.COLUMN_NAME_APPLICATION_NAME + " TEXT, " +
            UsageStatsContract.UsageStatsEntry.COLUMN_NAME_BEGIN_TIME + " TEXT, " +
            UsageStatsContract.UsageStatsEntry.COLUMN_NAME_END_TIME + " TEXT, " +
            UsageStatsContract.UsageStatsEntry.COLUMN_NAME_LAST_TIME_USED + " TEXT, " +
            UsageStatsContract.UsageStatsEntry.COLUMN_NAME_LAUNCH_COUNT + " INTEGER, " +
            UsageStatsContract.UsageStatsEntry.COLUMN_NAME_PACKAGE_NAME + " TEXT UNIQUE ON CONFLICT REPLACE) ";

    DBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USAGE_STAT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " +  UsageStatsContract.UsageStatsEntry.TABLE_NAME);
        onCreate(db);
    }

    /**
     * Inserts into the content provider
     * @param usageStatsList
     */
    public static void insertUsageStats(List<UsageStats> usageStatsList) {
        for(UsageStats usageStats : usageStatsList) {
            ContentValues values = new ContentValues();
            values.put(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_BEGIN_TIME, usageStats.getFirstTimeStamp());
            values.put(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_END_TIME, usageStats.getLastTimeStamp());
            values.put(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_LAST_TIME_USED, usageStats.getLastTimeUsed());
            values.put(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_PACKAGE_NAME, usageStats.getPackageName());
            values.put(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_APPLICATION_NAME,
                    UsageStatsManager.getInstance().getApplicationName(usageStats.getPackageName()));
            values.put(UsageStatsContract.UsageStatsEntry.COLUMN_NAME_LAUNCH_COUNT,
                    UsageStatsManager.getInstance().getLaunchCount(usageStats));

            Uri uri = MyAppStats.getAppContext().getContentResolver().insert(
                    UsageStatsContentProvider.CONTENT_URI, values);
        }
    }
}


