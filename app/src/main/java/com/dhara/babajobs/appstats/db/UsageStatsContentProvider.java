package com.dhara.babajobs.appstats.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import java.util.HashMap;

/**
 * Created by USER on 15-04-2016.
 */
public class UsageStatsContentProvider extends ContentProvider {
    public static final String PROVIDER_NAME = "com.dhara.babajobs.appstats";
    static final String URL = "content://" + PROVIDER_NAME + "/usage";
    public static final Uri CONTENT_URI = Uri.parse(URL);

    private static HashMap<String, String> FILES_PROJECTION_MAP;

    static final int APPS = 1;
    static final int APP_ID = 2;

    static final UriMatcher uriMatcher;
    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "usage", APPS);
        uriMatcher.addURI(PROVIDER_NAME, "usage/#", APP_ID);
    }

    /**
     * Database specific constant declarations
     */
    private SQLiteDatabase db;


    @Override
    public boolean onCreate() {
        Context context = getContext();
        DBHelper dbHelper = new DBHelper(context);

        /**
         * Create a writeable database which will trigger its
         * creation if it doesn't already exist.
         */
        db = dbHelper.getWritableDatabase();
        return (db == null)? false:true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(UsageStatsContract.UsageStatsEntry.TABLE_NAME);

        switch (uriMatcher.match(uri)) {
            case APPS:
                qb.setProjectionMap(FILES_PROJECTION_MAP);
                break;

            case APP_ID:
                qb.appendWhere( UsageStatsContract.UsageStatsEntry._ID + "=" + uri.getPathSegments().get(1));
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        if (sortOrder == null || sortOrder == ""){
            /**
             * By default sort on file names
             */
            sortOrder = UsageStatsContract.UsageStatsEntry.COLUMN_NAME_LAUNCH_COUNT + " DESC ";
        }
        Cursor c = qb.query(db,	projection,	selection, selectionArgs,null, null, sortOrder);

        /**
         * register to watch a content URI for changes
         */
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)){
            /**
             * Get all student records
             */
            case APPS:
                return "vnd.android.cursor.dir/vnd.dhara.babajobs.appstats.usage";

            /**
             * Get a particular student
             */
            case APP_ID:
                return "vnd.android.cursor.item/vnd.dhara.babajobs.appstats.usage";

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        /**
         * Add a new app usage record
         */
        long rowID = db.insert(UsageStatsContract.UsageStatsEntry.TABLE_NAME, "", values);

        /**
         * If record is added successfully
         */

        if (rowID > 0) {
            Uri _uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(_uri, null);
            return _uri;
        }
        throw new android.database.SQLException("Failed to add an app usage statistic " + uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
