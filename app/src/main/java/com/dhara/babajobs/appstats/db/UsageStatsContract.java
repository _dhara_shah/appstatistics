package com.dhara.babajobs.appstats.db;

import android.provider.BaseColumns;

/**
 * Created by USER on 14-04-2016.
 */
public class UsageStatsContract {
    public UsageStatsContract(){}

    public static abstract class UsageStatsEntry implements BaseColumns {
        public static final String TABLE_NAME="usage_stats_master";
        public static final String COLUMN_NAME_APPLICATION_NAME="app_name";
        public static final String COLUMN_NAME_PACKAGE_NAME="package_name";
        public static final String COLUMN_NAME_BEGIN_TIME="begin_timestamp";
        public static final String COLUMN_NAME_END_TIME="end_timestamp";
        public static final String COLUMN_NAME_LAST_TIME_USED="last_time_used";
        public static final String COLUMN_NAME_LAUNCH_COUNT="launch_count";
    }
}
