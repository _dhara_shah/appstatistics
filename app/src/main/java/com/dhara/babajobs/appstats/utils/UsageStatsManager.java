package com.dhara.babajobs.appstats.utils;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.app.usage.UsageStats;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.RemoteException;

import com.dhara.babajobs.appstats.MyAppStats;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by USER on 14-04-2016.
 */
public class UsageStatsManager {
    private static UsageStatsManager mUsageStatsManager;

    /**
     * Lazy creation of the UsageStatsManager class instance
     * @return
     */
    public static UsageStatsManager getInstance() {
        if(mUsageStatsManager == null) {
            mUsageStatsManager = new UsageStatsManager();
        }
        return mUsageStatsManager;
    }

    /**
     * Tries to get the app usage list for a short time to check if permissions are granted or not </bt>
     * This is a crude way of doing it.
     * @return
     */
    public List<UsageStats> getListToCheckPermissions() {
        return getAppList(System.currentTimeMillis()-50000, System.currentTimeMillis());
    }

    /**
     * Gets the app usage list for an year at least based on daily intervals </br>
     * @return
     */
    public List<UsageStats> getUsageStatList() {
        Calendar calendar = Calendar.getInstance();
        long endTime = calendar.getTimeInMillis();
        calendar.add(Calendar.YEAR, -2000);
        long startTime = calendar.getTimeInMillis();

        return getAppList(startTime, endTime);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private android.app.usage.UsageStatsManager getUsageStatsManager() {
        return (android.app.usage.UsageStatsManager) MyAppStats.getAppContext()
                .getSystemService(Context.USAGE_STATS_SERVICE);
    }

    /**
     * Gets the app usage list based on the starttime and endtime and daily interval
     * @param startTime
     * @param endTime
     * @return
     */
    private List<UsageStats> getAppList(long startTime, long endTime){
        android.app.usage.UsageStatsManager usageStatsManager = getUsageStatsManager();
        List<UsageStats> usageStatsList =
                usageStatsManager.queryUsageStats(android.app.usage.UsageStatsManager.INTERVAL_DAILY,
                        startTime, endTime);
        return usageStatsList;
    }

    /**
     * Gets the application name from the package name </br>
     * We get the package name from the usage stats class provided by android
     * @param packageName
     * @return
     */
    public String getApplicationName(String packageName){
        try {
            PackageManager packageManager = MyAppStats.getAppContext().getPackageManager();
            String appName = (String)packageManager
                    .getApplicationLabel(packageManager.getApplicationInfo(packageName,
                        PackageManager.GET_META_DATA));
            return appName;
        }catch (PackageManager.NameNotFoundException e) {
            return "Unknown";
        }
    }

    /**
     * mLaunchCount is hidden in the new UsageStats model class </br>
     * Thus, we use reflection to get the value of a particular package </br>
     * Though, this is not the best way to get a launch count,
     * as in the future the variable might be removed completely
     * @param usageStat
     * @return
     */
    public int getLaunchCount(UsageStats usageStat) {
        try {
            Field launchCountField = UsageStats.class.getDeclaredField("mLaunchCount");
            int launchCount = (Integer)launchCountField.get(usageStat);
            return launchCount;
        }catch (IllegalAccessException e){
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
